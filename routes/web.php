<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/* Gets all object of a class */
$app->get('orders', 'OrderController@index');
$app->get('retailers', 'RetailerController@index');
$app->get('roles', 'RoleController@index');
$app->get('users', 'UserController@index');


/* Finds a single object of a class by id */
$app->get('order/{orderId}', 'OrderController@find');
$app->get('retailer/{retailerId}', 'RetailerController@find');
$app->get('role/{roleId}', 'RoleController@find');
$app->get('user/{roleId}', 'UserController@find');


/* Find the role of a user */
$app->get('/user/{id}/role', 'UserController@role');

/* Find all orders made by a specified user */
$app->get('user/{id}/orders', 'OrderController@user');

/* Find all orders filtered by status */
$app->get('orders/status/{status}', 'OrderController@status');