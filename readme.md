Easy Odds API Task

How to:

My solution has 11 endpoints:

http://easyodds.app/orders - For fetching all orders

http://easyodds.app/retailers - For fetching all retailers

http://easyodds.app/roles - For fetching all roles

http://easyodds.app/users - For fetching all users

http://easyodds.app/order/{orderId} - For fetching a single order by id

http://easyodds.app/retailer/{retailerId} - For fetching a single retailer by id

http://easyodds.app/role/{roleId} - For fetching a single role by id

http://easyodds.app/user/{userId} - For fetching a single user by id

http://easyodds.app/user/{userId}/role - For fetching the role of a specified user

http://easyodds.app/user/{userId}/orders - For fetching all orders for a specified user

http://easyodds.app/orders/status/{status} - For fetching all orders with a specified status


Choices:

I chose to build my solution with Lumen. It's specifically designed for API development so I thought it a natural choice.


Misc:

Whe running the artisan commands to migrate/seed the database I've found I have to have the "DB_HOST" in the .env file set to 127.0.0.1. 
But then it needs to be set to localhost when making requests. Just a quirk of Homestead I suppose...