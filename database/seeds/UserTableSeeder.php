<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([

            'firstname' => 'Nicolas',
            'lastname' => 'Thompson',
            'email' => 'hire@nicolasthompson.com',
            'password' => 'badgers',
            'role' => 'guest'
        ]);

        User::create([

            'firstname' => 'Adam',
            'lastname' => 'Cornwell',
            'email' => 'hire@nicolasthompson.com',
            'password' => 'monkeys',
            'role' => 'admin'
        ]);
    }
}
