<?php

use Illuminate\Database\Seeder;
use App\Order;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::create([

            'userid' => '1',
            'retailerid' => '687628376',
            'status' => '1',
            'total' => '76455245'
        ]);

        Order::create([

            'userid' => '2',
            'retailerid' => '454325435',
            'status' => '0',
            'total' => '8768368'
        ]);
    }
}
