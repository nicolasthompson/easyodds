<?php

use Illuminate\Database\Seeder;
use App\Retailer;

class RetailerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Retailer::create([

            'name' => 'easyodds',
            'location' => 'London',
            'email' => 'hello@easyodds.com',
            'secret' => 'badgers_fly_south_for_the_winter',
        ]);

        Retailer::create([

            'name' => 'marcol',
            'location' => 'London',
            'email' => 'hello@marcol.com',
            'secret' => 'monkeys_fly_north_for_the_summer',
        ]);
    }
}
