<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{
    /**
     * Get all users
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = User::all();
        return response()->json($users);
    }

    /**
     * Find a single user by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function find($id)
    {
        $users = User::find($id);
        return response()->json($users);
    }

    /**
     * Find the role of a specified user
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function role($id)
    {
        $role = User::find($id);
        return response()->json($role->role);
    }
}
