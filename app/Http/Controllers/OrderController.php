<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Get all orders
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $orders = Order::all();
        return response()->json($orders);
    }

    /**
     * Find a single order by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function find($id)
    {
        $orders = Order::find($id);
        return response()->json($orders);
    }

    /**
     * Find orders by user
     *
     * @param $id
     * @return mixed
     */
    public function user($id)
    {
        return DB::table('order')->where('userid', '=', $id)->get();
    }

    /**
     * Find orders orders by status
     *
     * @param $status
     * @return mixed
     */
    public function status($status)
    {
        return DB::table('order')->where('status', '=', $status)->get();
    }
}
