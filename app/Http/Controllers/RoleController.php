<?php

namespace App\Http\Controllers;

use App\Role;

class RoleController extends Controller
{
    /**
     * Get all roles
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $roles = Role::all();
        return response()->json($roles);
    }

    /**
     * Find a single role by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function find($id)
    {
        $roles = Role::find($id);
        return response()->json($roles);
    }
}
