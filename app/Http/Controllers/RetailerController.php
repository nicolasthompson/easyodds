<?php

namespace App\Http\Controllers;

use App\Retailer;

class RetailerController extends Controller
{
    /**
     * Get all retailers
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $retailers = Retailer::all();
        return response()->json($retailers);
    }

    /**
     * Find a single retailer by id
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function find($id)
    {
        $retailer = Retailer::find($id);
        return response()->json($retailer);
    }
}
