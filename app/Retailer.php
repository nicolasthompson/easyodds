<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'location', 'email', 'secret'
    ];

    protected $table = 'retailer';
}
